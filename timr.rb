#!/usr/bin/env ruby
# frozen_string_literal: true

# Required environment variables:
# - TIMR_API_TOKEN=abc123

require 'faraday'
require 'json'
require 'pp'

# Query class performs the GraphQL call to retrieve info
class Query
  def initialize
    api_token = ENV['TIMR_API_TOKEN']
    init_api_connection api_token
  end

  def init_api_connection(token)
    @api_connection = Faraday.new(
      url: 'https://gitlab.com/api/graphql',
      headers: {
        'Content-Type' => 'application/json',
        'Authorization' => "Bearer #{token}"
      }
    )
  end

  def graphql_query_issues(group, after_date, author, state)
    <<~GRAPHQL
      {
        group(fullPath: "#{group}")
        {
          issues(
            authorUsername: "#{author}"
            #{state_dates(state)[:afterDate]}: "#{after_date}"
            state: #{state}
          )
          {
            count
            nodes {
              title
              webUrl
              #{state_dates(state)[:atDate]}
            }
          }
        }
      }
    GRAPHQL
  end

  def graphql_query_merge_requests(group, after_date, author, state)
    <<~GRAPHQL
      {
        group(fullPath: "#{group}")
        {
          mergeRequests(
            authorUsername: "#{author}"
            #{state_dates(state)[:afterDate]}: "#{after_date}"
            state: #{state}
          )
          {
            count
            nodes {
              title
              webUrl
              #{state_dates(state)[:atDate]}
            }
          }
        }
      }
    GRAPHQL
  end

  def run_issues(group, after_date, author, state)
    query = { query: graphql_query_issues(group, after_date, author, state) }
    response = @api_connection.post('', query.to_json)
    process_response response
  end

  def run_merge_requests(group, after_date, author, state)
    query = { query: graphql_query_merge_requests(group, after_date, author, state) }
    response = @api_connection.post('', query.to_json)
    process_response response
  end

  def process_response(response)
    if response.status != 200
      puts "Query failed #{response.status} - #{response.body}"
      data = nil
    else
      data = process_response_data(response)
    end
    data
  end

  def process_response_data(response)
    data = JSON.parse(response.body)
    data = nil if data.keys[0] == 'errors'
    data
  end
end

def ymd(utc)
  utc[0..9]
end

def state_dates(state)
  dates = {}
  dates['opened'] = { 'afterDate': 'createdAfter', 'atDate': 'createdAt' }
  dates['closed'] = { 'afterDate': 'closedAfter', 'atDate': 'closedAt' }
  dates['merged'] = { 'afterDate': 'mergedAfter', 'atDate': 'mergedAt' }
  dates[state]
end

def process_issues(data, author, state)
  return unless data && data['data']['group']

  issues = data['data']['group']['issues']
  issues['nodes'].each do |issue|
    puts "#{author} #{state} issue #{ymd(issue[state_dates(state)[:atDate]])} <a href='#{issue['webUrl']}'>#{issue['title']}</a><br>"
  end
end

def collect_issues(author, after_date)
  %w[gitlab-org gitlab-com].each do |group|
    %w[opened closed].each do |state|
      data = @query.run_issues group, after_date, author, state
      process_issues data, author, state
    end
  end
end

def process_merge_requests(data, author, state)
  return unless data && data['data']['group']

  merge_requests = data['data']['group']['mergeRequests']
  merge_requests['nodes'].each do |mr|
    puts "#{author} #{state} MR #{ymd(mr[state_dates(state)[:atDate]])} <a href='#{mr['webUrl']}'>#{mr['title']}</a><br>"
  end
end

def collect_merge_requests(author, after_date)
  %w[gitlab-org gitlab-com].each do |group|
    %w[opened merged].each do |state|
      data = @query.run_merge_requests group, after_date, author, state
      process_merge_requests data, author, state
    end
  end
end

if ARGV.length != 1
  puts 'Call the script with a start date, e.g: "ruby timr.rb 2021-10-14"'
  exit
end
after_date = ARGV[0]
puts "Report for issues and MRs since #{after_date}<br>"

@query = Query.new
File.readlines('team.txt', chomp: true).each do |line|
  author = line
  next unless author.length.positive?

  collect_issues author, after_date
  collect_merge_requests author, after_date
end
